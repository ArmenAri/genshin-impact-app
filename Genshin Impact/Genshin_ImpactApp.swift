//
//  Genshin_ImpactApp.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import SwiftUI

@main
struct Genshin_ImpactApp: App {
    @StateObject private var characterModelData = CharacterModelData()
    @StateObject private var artifactModelData = ArtifactModelData()
    @StateObject private var weaponModelData = WeaponModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(characterModelData)
                .environmentObject(artifactModelData)
                .environmentObject(weaponModelData)
        }
    }
}
