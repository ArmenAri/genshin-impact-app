//
//  Artifact.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import Foundation

struct Artifact: Hashable, Codable {
    private enum CodingKeys: String, CodingKey {
        case name, max_rarity
        
        case two_piece_bonus = "2-piece_bonus"
        case four_piece_bonus = "4-piece_bonus"
    }
    
    var name: String
    var max_rarity: Int
    var two_piece_bonus: String
    var four_piece_bonus: String
    
    var iconUrl: String {
        get {
            return "https://www.gensh.in/fileadmin/Database/Artifacts/With_Background/af_" + self.normalizedName + ".jpg"
        }
    }
    
    var normalizedName: String {
        get {
            return self.name.lowercased().replacingOccurrences(of: "'", with: "").split(separator: " ").joined(separator: "_")
        }
    }
}

extension Artifact: Identifiable {
    var id: UUID { return UUID() }
    
}
