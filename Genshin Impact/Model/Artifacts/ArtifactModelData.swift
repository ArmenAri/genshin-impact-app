//
//  ArtifactModelData.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import Foundation

final class ArtifactModelData: ObservableObject {
    @Published var artifacts: [Artifact] = [Artifact]()
    
    func getArtifactsByRarity(rarity: Int) -> [Artifact] {
        return artifacts.filter { artifact in
            return artifact.max_rarity == rarity
        }
    }
    
    init() {
        let url = URL(string: "https://api.genshin.dev/artifacts/all")!
        
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            do {
                if let artifactData = data {
                    let decodedData = try JSONDecoder().decode([Artifact].self, from: artifactData)
                    DispatchQueue.main.async {
                        self.artifacts = decodedData
                    }
                } else {
                    print("Pas de données !")
                }
            } catch {
                print("Erreur !")
            }
        }.resume()
    }
}
