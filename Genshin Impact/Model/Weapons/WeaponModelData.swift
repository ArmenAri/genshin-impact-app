//
//  WeaponModelData.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import Foundation

final class WeaponModelData: ObservableObject {
    @Published var weapons: [Weapon] = [Weapon]()
    
    func getWeaponsByRarity(rarity: Int) -> [Weapon] {
        return weapons.filter { weapon in
            return weapon.rarity == rarity
        }
    }
    
    init() {
        let url = URL(string: "https://api.genshin.dev/weapons/all")!
        
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            do {
                if let weaponData = data {
                    let decodedData = try JSONDecoder().decode([Weapon].self, from: weaponData)
                    DispatchQueue.main.async {
                        self.weapons = decodedData
                    }
                } else {
                    print("Pas de données !")
                }
            } catch {
                print("Erreur !")
            }
        }.resume()
    }
}
