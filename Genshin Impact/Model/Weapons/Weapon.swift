//
//  Weapon.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import Foundation

struct Weapon: Hashable, Codable {
    var name: String
    var type: String
    var rarity: Int
    var baseAttack: String
    var subStat: String
    var passiveName: String
    var passiveDesc: String
    var location: String
    
    var iconUrl: String {
        get {
            return "https://www.gensh.in/fileadmin/Database/Weapons/" + self.type + "/" + String(self.rarity) + "_" + self.normalizedName + ".jpg"
        }
    }
    
    var normalizedName: String {
        get {
            return self.name.lowercased().replacingOccurrences(of: "'", with: "").split(separator: " ").joined(separator: "_")
        }
    }
    
}

extension Weapon: Identifiable {
  var id: UUID { return UUID() }
}
