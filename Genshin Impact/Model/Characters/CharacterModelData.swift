//
//  ModelData.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import Foundation

final class CharacterModelData: ObservableObject {
    @Published var characters: [Character] = [Character]()
    
    func getCharactersByVision(vision: String) -> [Character] {
        return characters.filter { character in
            return character.vision == vision
        }.sorted { (a, b) -> Bool in
            a.rarity < b.rarity
        }
    }
    
    init() {
        let url = URL(string: "https://api.genshin.dev/characters/all")!
        
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            do {
                if let characterData = data {
                    var decodedData = try JSONDecoder().decode([Character].self, from: characterData)
                    DispatchQueue.main.async {
                        self.characters = self.processList(&decodedData)
                    }
                } else {
                    print("Pas de données !")
                }
            } catch {
                print("Erreur !")
            }
        }.resume()
    }
    
    func processList(_ data: inout [Character]) -> [Character] {
        let incompleteNames: [String] = [
            "Beidou", "Bennett", "Traveler", "Ganyu", "Mona"
        ]
        for incNam in incompleteNames {
            data.removeAll { $0.name == incNam }
        }
        return data
    }
}
