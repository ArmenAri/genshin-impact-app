//
//  Character.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import Foundation

struct Character: Hashable, Codable {
    var name: String
    var description: String
    var vision: String
    var weapon: String
    var rarity: Int
    
    var portraitUrl: String {
        get {
            return "https://www.gensh.in/fileadmin/Database/Characters/" + self.name + "/CCard_" + self.name + ".png"
        }
    }
    
    var iconUrl: String {
        get {
            return "https://api.genshin.dev/characters/" + self.name.lowercased() + "/icon"
        }
    }
    
    var elementIconUrl: String {
        get {
            return "https://api.genshin.dev/elements/" + self.vision.lowercased() + "/icon"
        }
    }
}

extension Character: Identifiable {
  var id: UUID { return UUID() }
}
