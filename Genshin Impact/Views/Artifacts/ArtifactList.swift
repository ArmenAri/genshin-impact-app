//
//  ArtifactList.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import SwiftUI

struct ArtifactList: View {
    @EnvironmentObject var fetchedArtifacts: ArtifactModelData
    var gridItems: [GridItem] = [GridItem(), GridItem()]
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical) {
                VStack(alignment: .leading){
                    ArtifactCarousel(title: "3 ✦", artifacts: fetchedArtifacts.getArtifactsByRarity(rarity: 3))
                    ArtifactCarousel(title: "4 ✦", artifacts: fetchedArtifacts.getArtifactsByRarity(rarity: 4))
                    ArtifactCarousel(title: "5 ✦", artifacts: fetchedArtifacts.getArtifactsByRarity(rarity: 5))
                }
                .padding()
                .navigationTitle("Artifacts")
            }
        }
    }
}

struct ArtifactList_Previews: PreviewProvider {
    static var previews: some View {
        ArtifactList()
                .environmentObject(ArtifactModelData())
        }
}
