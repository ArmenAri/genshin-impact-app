//
//  ArtifactInfoOverlay.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import SwiftUI

struct ArtifactInfoOverlay: View {
    let artifact: Artifact
    let fontName: String = "HYWenHei 85W"
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading){
                Text(artifact.name)
                    .foregroundColor(.white)
                    .font(.custom(fontName, size: 20))
                Text(String(artifact.max_rarity) + " ✦ ")
                    .foregroundColor(.white)
                    .font(.custom(fontName, size: 16))
            }
        }
        .padding(6)
        .opacity(0.8)
    }
}

struct ArtifactInfoOverlay_Previews: PreviewProvider {
    static var artifact: Artifact = Artifact(name: "Berserker", max_rarity: 4, two_piece_bonus: "two_piece_bonus", four_piece_bonus: "four_piece_bonus")
    static var previews: some View {
        ArtifactInfoOverlay(artifact: artifact)
    }
}
