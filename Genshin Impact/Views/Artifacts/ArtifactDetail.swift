//
//  ArtifactDetail.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import SwiftUI

struct ArtifactDetail: View {
    var artifact: Artifact
    var font: String = "HYWenHei 85W"
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                HStack {
                    Icon(url: artifact.iconUrl)
                    VStack {
                        Text(String(repeating: " ✦ ", count: artifact.max_rarity))
                            .font(.custom(font, size: 22))
                            .multilineTextAlignment(.center)
                    }.padding()
                }.padding(.all, 20.0)
                
                Divider()
                Text(artifact.name)
                    .font(.custom(font, size: 26))
                    .foregroundColor(.primary)
                    .padding(.bottom, 5)
                Text("Two Piece Bonus")
                    .font(.custom(font, size: 24))
                    .multilineTextAlignment(.leading)
                Text(artifact.two_piece_bonus)
                    .font(.custom(font, size: 20))
                    .foregroundColor(.secondary)
                Text("Four Piece Bonus")
                    .font(.custom(font, size: 24))
                    .multilineTextAlignment(.leading)
                Text(artifact.four_piece_bonus)
                    .font(.custom(font, size: 20))
                    .foregroundColor(.secondary)
                
            }
            .navigationTitle(artifact.name)
            .navigationBarTitleDisplayMode(.inline)
        }
        .padding()
    }
}

struct ArtifactDetail_Previews: PreviewProvider {
    static var artifact: Artifact = Artifact(name: "Berserker", max_rarity: 4, two_piece_bonus: "two_piece_bonus", four_piece_bonus: "four_piece_bonus")
    
    static var previews: some View {
            ArtifactDetail(artifact: artifact)
        }
}
