//
//  ArtifactCard.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import SwiftUI

struct ArtifactCard: View {
    var artifact: Artifact
    
    var body: some View {
        AsyncImage(url: URL(string: artifact.iconUrl)!,
                   placeholder: { ProgressView() })
            .aspectRatio(contentMode: .fit)
            .overlay(ArtifactInfoOverlay(artifact: artifact), alignment: .topLeading)
            .cornerRadius(10).frame(width: 180, height: 180, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .contextMenu
            {
                Text("Name : " + artifact.name)
                Text("Rariry : " + String(repeating: " ✦ ", count: artifact.max_rarity))
                
                Link("More about " + artifact.name, destination: URL(string: "https://genshin.gg/artifacts")!)
            }
        
    }
}


struct ArtifactCard_Previews: PreviewProvider {
    static var artifact: Artifact = Artifact(name: "Berserker", max_rarity: 4, two_piece_bonus: "two_piece_bonus", four_piece_bonus: "four_piece_bonus");
    
    static var previews: some View {
        ArtifactCard(artifact: artifact)
    }
}
