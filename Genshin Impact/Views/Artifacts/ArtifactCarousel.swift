//
//  ArtifactCarousel.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 20/01/2021.
//

import SwiftUI

struct ArtifactCarousel: View {
    var title: String
    var artifacts: [Artifact]
    var font: String = "HYWenHei 85W"
    
    var body: some View {
        Text(title)
            .font(.custom(font, size: 26))
            .foregroundColor(.primary)
            .padding(.bottom, 5)
        ScrollView(.horizontal) {
            HStack(spacing: 10) {
                ForEach(artifacts, id: \.self) { artifact in
                    NavigationLink(destination: ArtifactDetail(artifact: artifact)) {
                        ArtifactCard(artifact: artifact)
                    }
                }
            }
        }.frame(height: 200)
    }
}
