//
//  Icon.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import SwiftUI

struct Icon: View {
    var url: String
    
    var body: some View {
        AsyncImage(url: URL(string: url)!,
                   placeholder: { ProgressView() })
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.white, lineWidth: 4))
            .shadow(radius: 7)
            .aspectRatio(contentMode: .fit)
            .frame(width: 128, alignment: .center)

    }
}

struct Icon_Previews: PreviewProvider {
    static var previews: some View {
        Icon(
            url: "https://api.genshin.dev/characters/albedo/icon"
        )
    }
}
