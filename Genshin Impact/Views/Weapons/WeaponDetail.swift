//
//  WeaponDetail.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import SwiftUI

struct WeaponDetail: View {
    var weapon: Weapon
    var font: String = "HYWenHei 85W"
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Text(weapon.name)
                    .font(.custom(font, size: 28))
                
                AsyncImage(url: URL(string: weapon.iconUrl)!,
                           placeholder: { ProgressView() })
                    .aspectRatio(contentMode: .fit)

                Divider()
                Text("Location : \(weapon.location)")
                    .font(.custom(font, size: 20))
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                Divider()
                
                Text(weapon.passiveName)
                    .font(.custom(font, size: 28))
                
                Text(weapon.passiveDesc)
                    .font(.custom(font, size: 20))
                    .lineLimit(100)
            }
            .padding(5)
        }
        .navigationTitle(weapon.name)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct WeaponDetail_Previews: PreviewProvider {
    static var weapon: Weapon = Weapon(
        name: "Iron Sting",
        type: "Sword",
        rarity: 4,
        baseAttack: "CRIT DMG",
        subStat: "CRIT DMG",
        passiveName: "Press the Advantage",
        passiveDesc: "After defeating an enemy, ATK is increased by 12/15/18/21/24% for 30s. This effect has a maximum of 3 stacks, and the duration of each stack is independent of the others.",
        location: "Starglitter Exchange")
    static var previews: some View {
        WeaponDetail(weapon: weapon)
    }
}
