//
//  WeaponList.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import SwiftUI

struct WeaponList: View {
    @EnvironmentObject var fetchedWeapons: WeaponModelData
    var gridItems: [GridItem] = [GridItem()]
    
    var body: some View {
        /*NavigationView {
         ScrollView(.vertical) {
         LazyVGrid(columns: gridItems, spacing: 10) {
         ForEach(fetchedWeapons.weapons.sorted(by: { (a, b) -> Bool in
         a.rarity < b.rarity
         }), id: \.self) { weapon in
         NavigationLink(destination: WeaponDetail(weapon: weapon)) {
         WeaponCard(weapon: weapon)
         }
         }
         }.padding(10)
         }
         .navigationTitle("Weapons")
         }*/
        NavigationView {
            ScrollView(.vertical) {
                VStack(alignment: .leading){
                    WeaponCarousel(title: "1 ✦", weapons: fetchedWeapons.getWeaponsByRarity(rarity: 1))
                    WeaponCarousel(title: "2 ✦", weapons: fetchedWeapons.getWeaponsByRarity(rarity: 2))
                    WeaponCarousel(title: "3 ✦", weapons: fetchedWeapons.getWeaponsByRarity(rarity: 3))
                    WeaponCarousel(title: "4 ✦", weapons: fetchedWeapons.getWeaponsByRarity(rarity: 4))
                    WeaponCarousel(title: "5 ✦", weapons: fetchedWeapons.getWeaponsByRarity(rarity: 5))
                }
                .padding()
                .navigationTitle("Weapons")
            }
        }
    }
}

struct WeaponList_Previews: PreviewProvider {
    static var previews: some View {
        WeaponList().environmentObject(WeaponModelData())
    }
}
