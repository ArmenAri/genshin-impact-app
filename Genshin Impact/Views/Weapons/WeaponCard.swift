//
//  WeaponCard.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import SwiftUI

struct WeaponCard: View {
    var weapon: Weapon
    
    var body: some View {
        AsyncImage(url: URL(string: weapon.iconUrl)!,
                   placeholder: { ProgressView() })
            .frame(width: 300.0, height: 170.0)
            .aspectRatio(contentMode: .fit)
            .cornerRadius(10)
            .contextMenu
            {
                Text("Name : " + weapon.name)
                Text("Type : " + weapon.type)
                Text("Rariry : " + String(repeating: " ✦ ", count: weapon.rarity))
                Text("Base Attack : " + weapon.baseAttack)
                Text("Substat : " + weapon.subStat)
                
                Link("More about " + weapon.name, destination: URL(string: "https://genshin.gg/weapons")!)
            }
    }
}

struct WeaponCard_Previews: PreviewProvider {
    static var weapon: Weapon = Weapon(
        name: "Sharpshooter's Oath",
        type: "Bow",
        rarity: 3,
        baseAttack: "39",
        subStat: "CRIT DMG",
        passiveName: "",
        passiveDesc: "",
        location: "Chest")
    
    static var previews: some View {
        WeaponCard(weapon: weapon)
    }
}
