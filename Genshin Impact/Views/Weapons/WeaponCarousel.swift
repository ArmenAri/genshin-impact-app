//
//  WeaponCarousel.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 20/01/2021.
//

import SwiftUI

struct WeaponCarousel: View {
    var title: String
    var weapons: [Weapon]
    var font: String = "HYWenHei 85W"
    
    var body: some View {
        Text(title)
            .font(.custom(font, size: 26))
            .foregroundColor(.primary)
            .padding(.bottom, 5)
        ScrollView(.horizontal) {
            HStack(spacing: 10) {
                ForEach(weapons, id: \.self) { weapon in
                    NavigationLink(destination: WeaponDetail(weapon: weapon)) {
                        WeaponCard(weapon: weapon)
                    }
                }
            }
        }.frame(height: 160)
    }
}
