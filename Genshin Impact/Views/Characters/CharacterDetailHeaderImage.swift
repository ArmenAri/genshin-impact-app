//
//  HeaderImageBG.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 18/01/2021.
//

import SwiftUI

struct CharacterDetailHeaderImage: View {
    var url: String
    var body: some View {
        AsyncImage(url: URL(string: url)!,
                   placeholder: { ProgressView() })
            .blur(radius: 3)
            .position(x: 300, y: 0)
    }
}

struct HeaderImageBG_Previews: PreviewProvider {
    static var previews: some View {
        CharacterDetailHeaderImage(url: "https://api.genshin.dev/elements/geo/icon")
    }
}
