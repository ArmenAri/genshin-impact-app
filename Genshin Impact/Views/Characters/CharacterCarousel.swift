//
//  CharacterCarousel.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 20/01/2021.
//

import SwiftUI

struct CharacterCarousel: View {
    var title: String
    var characters: [Character]
    var font: String = "HYWenHei 85W"
    
    var body: some View {
        Text(title)
            .font(.custom(font, size: 26))
            .foregroundColor(.primary)
            .padding(.bottom, 5)
        ScrollView(.horizontal) {
            HStack(spacing: 5) {
                ForEach(characters, id: \.self) { character in
                    NavigationLink(destination: CharacterDetail(character: character)) {
                        CharacterCard(character: character)
                    }
                }
            }
        }.frame(height: 100)
    }
}
