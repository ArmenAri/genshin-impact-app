//
//  CharacterCard.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import SwiftUI

struct CharacterCard: View {
    var character: Character
    
    var body: some View {
        AsyncImage(url: URL(string: character.portraitUrl)!,
                   placeholder: { ProgressView() })
            .frame(width: 310.0, height: 110.0)
            .overlay(CharacterInfoOverlay(character: character), alignment: .topLeading)
            .background(Image("background")
                            .resizable()
                            .opacity(0.59))
            .background(Color.gray)
            .cornerRadius(7)
            .aspectRatio(contentMode: .fit)
            .contextMenu
            {
                Text("Name : " + character.name)
                Text("Vision : " + character.vision)
                Text("Rariry : " + String(repeating: " ✦ ", count: character.rarity))
                Text("Weapon : " + character.weapon)
                
                Link("More about " + character.name, destination: URL(string: "https://genshin.gg/characters/" + character.name)!)
            }
    }
}

struct CharacterCard_Previews: PreviewProvider {
    static var character: Character = Character(
        name: "Albedo",
        description: "Albedo's Description",
        vision: "Geo",
        weapon: "Sword",
        rarity: 4)
    
    static var previews: some View {
        CharacterCard(character: character)
    }
}

