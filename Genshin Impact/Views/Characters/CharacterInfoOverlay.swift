//
//  CharacterInfoOverlay.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import SwiftUI

struct CharacterInfoOverlay: View {
    let character: Character
    let fontName: String = "HYWenHei 85W"
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading){
                Text(character.name)
                    .foregroundColor(.white)
                    .font(.custom(fontName, size: 26))
                Text(String(character.rarity)  + " ✦ " + character.vision)
                    .foregroundColor(.white)
                    .font(.custom(fontName, size: 20))
                AsyncImage(url: URL(string: character.elementIconUrl)!,
                           placeholder: { ProgressView() })
                    .aspectRatio(contentMode: .fit)
            }
        }
        .padding(6)
        .opacity(0.8)
    }
}
