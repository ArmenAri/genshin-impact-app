//
//  CharacterList.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import SwiftUI

struct CharacterList: View {
    @EnvironmentObject var fetchedCharacters: CharacterModelData
    var gridItems: [GridItem] = [GridItem()]
    var font: String = "HYWenHei 85W"
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical) {
                VStack(alignment: .leading){
                    CharacterCarousel(title: "Anemo", characters: fetchedCharacters.getCharactersByVision(vision: "Anemo"))
                    CharacterCarousel(title: "Geo", characters: fetchedCharacters.getCharactersByVision(vision: "Geo"))
                    CharacterCarousel(title: "Pyro", characters: fetchedCharacters.getCharactersByVision(vision: "Pyro"))
                    CharacterCarousel(title: "Cryo", characters: fetchedCharacters.getCharactersByVision(vision: "Cryo"))
                    CharacterCarousel(title: "Electro", characters: fetchedCharacters.getCharactersByVision(vision: "Electro"))
                    CharacterCarousel(title: "Hydro", characters: fetchedCharacters.getCharactersByVision(vision: "Hydro"))
                    
                }
                .padding()
                .navigationTitle("Characters")
            }
        }
    }
}

struct CharacterList_Previews: PreviewProvider {
    static var previews: some View {
            CharacterList()
                .environmentObject(CharacterModelData())
        }
}
