//
//  CharacterDetail.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import SwiftUI

struct CharacterDetail: View {
    var character: Character
    var font: String = "HYWenHei 85W"
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                CharacterDetailHeaderImage(url: character.elementIconUrl)
                    .frame(width: 200, height: 225)
                    .ignoresSafeArea(edges: .top)
                
                Icon(
                    url: character.iconUrl
                )
                .offset(y: -130.0)
                .padding(.bottom, -100)
                
                Text(character.name)
                    .font(.custom(font, size: 30))
                    .foregroundColor(.primary)

                HStack {
                    Text(character.vision)
                        .font(.custom(font, size: 20))
                    Spacer()
                    Text("Weapon : " + character.weapon)
                        .font(.custom(font, size: 20))
                }
                .font(.subheadline)
                .foregroundColor(.secondary)

                Divider()

                Text("About \(character.name)")
                    .font(.custom(font, size: 28))
                Text(character.description)
                    .font(.custom(font, size: 20))
                    .lineLimit(100)
            }
            .padding()
        }
        .navigationTitle(character.name)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct CharacterDetail_Previews: PreviewProvider {
    static var character: Character = Character(
        name: "Albedo",
        description: "A mysterious guest invited by the Wangsheng Funeral Parlor. Extremely knowledgeable in all things.",
        vision: "Geo",
        weapon: "Sword",
        rarity: 5)
    static var previews: some View {
            CharacterDetail(character: character)
        }
}
