//
//  ContentView.swift
//  Genshin Impact
//
//  Created by Armen Aristakesyan on 11/01/2021.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        TabView{
            CharacterList()
                .tabItem {
                    Image(systemName: "person.fill")
                    Text("Characters")
              }
            ArtifactList()
                .tabItem {
                    Image(systemName: "sparkles")
                    Text("Artifacts")
              }
            WeaponList()
                .tabItem {
                    Image(systemName: "wand.and.rays")
                    Text("Weapons")
              }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(CharacterModelData())
            .environmentObject(ArtifactModelData())
            .environmentObject(WeaponModelData())
    }
}
